const puppeteer = require('puppeteer');
const cheerio = require('cheerio');
const mongoose = require('mongoose');

// Connect to your MongoDB database
mongoose.connect('mongodb://127.0.0.1:27017/company_data', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

// Define the Mongoose schema
const companySchema = new mongoose.Schema({
  name: String,
  about: String,
  industry: String,
  employeeCount: String,
});

const Company = mongoose.model('Company', companySchema);

async function scrapeCompanyData(link) {
  const browser = await puppeteer.launch({
    headless: false, // For testing, you can enable/disable headless mode
    userDataDir: '/home/anto/Downloads/MyChromeDir',
  });

  const page = await browser.newPage();
  await page.setViewport({ width: 1000, height: 926 });

  async function scrapeCompany() {
    await page.goto(link, { waitUntil: 'networkidle2' });
    const html = await page.content();
    const $ = cheerio.load(html);

    const name = $(".ember-view.text-display-medium-bold.org-top-card-summary__title.full-width").text().trim();
    const about = $('.break-words.white-space-pre-wrap.t-black--light.text-body-medium').text().trim();
    const industry = $('<dd class="mb4.t-black--light.text-body-medium">IT Services and IT Consulting</dd>').text().trim()
    const employeeCount = $('.t-black--light.text-body-medium.mb1').text().trim();

    const companyData = {
      name,
      about,
      industry,
      employeeCount,
    };

    return companyData;
  }

  try {
    const companyData = await scrapeCompany();
    console.log(companyData);

    // Save the scraped data to MongoDB using Mongoose
    const newCompany = new Company({
      name: companyData.name,
      about: companyData.about,
      industry: companyData.industry,
      employeeCount: companyData.employeeCount,
    });
    await newCompany.save();
    console.log('Company data saved to MongoDB successfully!');
  } catch (error) {
    console.error('Error:', error.message);
  } finally {
    browser.close();
  }
}


// Example usage: Input the LinkedIn profile link
const linkedinProfileLink = 'https://www.linkedin.com/company/hcltech/about/';
// const linkedinProfileLink = 'https://www.linkedin.com/company/infosys/about/'
scrapeCompanyData(linkedinProfileLink);

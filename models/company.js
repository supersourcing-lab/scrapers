const mongoose = require('mongoose');
const companySchema = new mongoose.Schema({
    name: { type: String },
    about: { type: String },
    Website: { type: String }, // Add other properties as necessary
    Industry: { type: String },
    'Company size': { type: String },
  });

const Company = mongoose.model('Company', companySchema);

module.exports = Company;

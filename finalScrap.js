const puppeteer = require("puppeteer");
const cheerio = require("cheerio");
const mongoose = require("mongoose");
const company = require("./models/company");

mongoose
  .connect("mongodb://127.0.0.1:27017/finalComapny", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Connected to MongoDB");
  })
  .catch((error) => {
    console.error("Error connecting to MongoDB:", error);
  });

async function scrapeCompanyData(link) {
  const browser = await puppeteer.launch({
    headless: false, // For testing, you can enable/disable headless mode
    userDataDir: "/home/anto/Downloads/MyChromeDir",
  });

  const page = await browser.newPage();
  await page.setViewport({ width: 1000, height: 926 });
  await page.setDefaultNavigationTimeout(120000);

  const visitedLinks = new Set(); // Set to store visited links

  async function scrapeCompany(page, link) {
    await page.goto(link, { waitUntil: "networkidle2" });
    const html = await page.content();
    const $ = cheerio.load(html);

    const h1ElementXPath =
      "/html/body/div[4]/div[3]/div/div[2]/div/div[2]/main/div[1]/section/div/div[2]/div[2]/div[1]/div[2]/div/h1";
    const alternativeXPath1 =
      "/html/body/div[5]/div[3]/div/div[2]/div/div[2]/main/div[1]/section/div/div[2]/div[1]/div[1]/div[2]/div/h1";
    const alternativeXPath2 =
      "/html/body/div[4]/div[3]/div/div[2]/div/div[2]/main/div[1]/section/div/div[2]/div[1]/div[1]/div[2]/div/h1";
    const alternativeXPath3 =
      "/html/body/div[5]/div[3]/div/div[2]/div/div[2]/main/div[1]/section/div/div[2]/div[1]/div[1]/div[2]/div/h1"; // Add your third alternative XPath here
    const name = await page.evaluate(
      async (h1XPath, altXPath1, altXPath2, altXPath3) => {
        try {
          const h1Element = document.evaluate(
            h1XPath,
            document,
            null,
            XPathResult.FIRST_ORDERED_NODE_TYPE,
            null
          ).singleNodeValue;

          if (!h1Element) {
            // If the element is not found using the primary XPath, try the alternative ones
            const altElement1 = document.evaluate(
              altXPath1,
              document,
              null,
              XPathResult.FIRST_ORDERED_NODE_TYPE,
              null
            ).singleNodeValue;

            const altElement2 = document.evaluate(
              altXPath2,
              document,
              null,
              XPathResult.FIRST_ORDERED_NODE_TYPE,
              null
            ).singleNodeValue;

            const altElement3 = document.evaluate(
              altXPath3,
              document,
              null,
              XPathResult.FIRST_ORDERED_NODE_TYPE,
              null
            ).singleNodeValue;

            return altElement1
              ? altElement1.getAttribute("title").trim()
              : altElement2
              ? altElement2.getAttribute("title").trim()
              : altElement3
              ? altElement3.getAttribute("title").trim()
              : "";
          }

          return h1Element.getAttribute("title").trim();
        } catch (error) {
          console.error(
            "Error occurred while trying to retrieve the element:",
            error
          );
          return "";
        }
      },
      h1ElementXPath,
      alternativeXPath1,
      alternativeXPath2,
      alternativeXPath3
    );
    const aboutElementXPath =
      "/html/body/div[4]/div[3]/div/div[2]/div/div[2]/main/div[2]/div/div/div[1]/section/p";
    const about = await page.evaluate(async (XPath) => {
      try {
        const aboutelement = document.evaluate(
          XPath,
          document,
          null,
          XPathResult.FIRST_ORDERED_NODE_TYPE,
          null
        ).singleNodeValue;
        return aboutelement.textContent.trim();
      } catch (error) {
        console.error(
          "Error occurred while trying to retrieve the element:",
          error
        );
        return "";
      }
    }, aboutElementXPath);
    // Find the description list using XPath
    const dlElement = await page.$x(
      "/html/body/div[4]/div[3]/div/div[2]/div/div[2]/main/div[2]/div/div/div[1]/section/dl"
    );

    // Create key-value pairs from the description list (dl)
    const companyData = {
      name,
      about, // Assign the extracted 'name' and 'about' to the 'companyData' object
    };

    if (dlElement.length > 0) {
      const dtElements = await dlElement[0].$$("dt");
      const ddElements = await dlElement[0].$$("dd");
      for (let i = 0; i < dtElements.length; i++) {
        let dtText = await page.evaluate(
          (el) => el.textContent.trim(),
          dtElements[i]
        );
        let ddText = await page.evaluate(
          (el) => el.textContent.trim(),
          ddElements[i]
        );
        // Remove unnecessary spaces and \n characters from dtText and ddText
        dtText = dtText.replace(/['":]/g, "").trim();
        ddText = ddText.replace(/\n/g, " ").trim(); // Replace newline with space
        // Skip adding the key-value pair if dtText is 'Phone', 'Headquarters', 'Founded', or 'Specialties'
        if (
          dtText !== "Phone" &&
          dtText !== "Headquarters" &&
          dtText !== "Founded" &&
          dtText !== "Specialties"
        ) {
          // If all conditions are met, add the 'dtText' as a property to 'companyData'
          // and assign its corresponding 'ddText' value to it.
          companyData[dtText] = ddText;
        }
      }
    }

    // Wait for the "People also viewed" section to load with increased timeout
    const timeout = 60000; // 60 seconds
    await page.waitForSelector(".app-aware-link");

    // Using class name to select all links in the "People also viewed" section
    const relatedlinks = await page.evaluate(() => {
      const links = new Set();
      const relatedLinksElements = document.querySelectorAll(".app-aware-link");
      relatedLinksElements.forEach((element) => {
        links.add(element.href);
      });
      return Array.from(links); // Convert set to array
    });

    // Filter out unwanted links by keeping only those matching the specified pattern
    const pattern = /^https:\/\/www\.linkedin\.com\/company\/[a-zA-Z0-9-]+\/$/;
    const filteredLinks = relatedlinks.filter((link) => pattern.test(link));
    companyData.relatedlinks = filteredLinks;

    return companyData;
  }
    

  async function saveCompanyToDatabase(data) {
    try {
      // Save the companyData to MongoDB using the Company model
      const newCompany = new company(data);
      await newCompany.save();
      console.log("Data saved successfully!");
    } catch (error) {
      console.error("Error saving data to MongoDB:", error);
    }
  }

  async function scrapeRelatedLinks(links) {
    for (const link of links) {
      const linkAbout = link + "/about"; // Append '/about' to the link
      if (!visitedLinks.has(linkAbout)) {
        console.log(`Scraping data from link: ${linkAbout}`);
        const linkData = await scrapeCompany(page, linkAbout);
        console.log(linkData);
        visitedLinks.add(linkAbout);

        // Save the data for the current company to MongoDB
        await saveCompanyToDatabase(linkData);

        // Recursively scrape data for related links
        await scrapeRelatedLinks(linkData.relatedlinks);
      }
    }
  }

  const initialCompanyData = await scrapeCompany(page, link);
  console.log(initialCompanyData);

  // Save the data for the initial company to MongoDB
  await saveCompanyToDatabase(initialCompanyData);

  // Start scraping related links
  await scrapeRelatedLinks(initialCompanyData.relatedlinks);

  browser.close();
}

// Example usage: Input the LinkedIn profile link
const linkedinProfileLink = "https://www.linkedin.com/company/infosys";
scrapeCompanyData(linkedinProfileLink);
